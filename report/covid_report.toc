\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Short term forecasts}{2}{section.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1}BIOCOMSC-Gompertz}{2}{subsection.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2}DMSPG-Bayes}{2}{subsection.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {1.2.1}SIR with reporting delay}{2}{subsubsection.1.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {1.2.2}Parameter estimation}{3}{subsubsection.1.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {1.2.3}Longer prediction including policy changes}{3}{subsubsection.1.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3}Forschungszentrum Jülich and Frankfurt Institute for Advanced Studies}{4}{subsection.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.4}SIkJ$\alpha $-model}{4}{subsection.1.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {1.4.1}Baseline assumptions}{4}{subsubsection.1.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {1.4.2}Parameter Estimation (simplified model)}{5}{subsubsection.1.4.2}%
