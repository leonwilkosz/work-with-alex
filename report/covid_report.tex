\documentclass[11pt]{scrartcl}


\usepackage[
backend=biber,
style=alphabetic,
sorting=ynt
]{biblatex}
\addbibresource{biblio1.bib}
\usepackage[utf8]{inputenc} 
\usepackage[T1]{fontenc}
\usepackage{a4wide}
\usepackage{lmodern}
\usepackage[english]{babel}
\usepackage{mathrsfs}
\usepackage{etex}
\usepackage{mathtools}
\usepackage{latexsym}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{caption}
\usepackage{mathabx}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[section]{placeins}	
\usepackage{hyperref}
\usepackage[nameinlink]{cleveref}
\usepackage{afterpage}

\usepackage{listings}




\usepackage[all]{xy}
\usepackage{verbatim}
\usepackage{listings}
\usepackage{fancyvrb}

\usepackage{graphicx}

\usepackage[dvipsnames]{xcolor}
\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}
\usepackage{colortbl}
\usepackage{accents} %% \undertilde
\usepackage{enumerate}
\usepackage{wrapfig}
\usepackage{tikz}
\usepackage{tikz-cd}
\usetikzlibrary{automata,shapes,arrows,matrix,backgrounds,positioning,plotmarks,calc,patterns,matrix,decorations.pathreplacing,decorations.pathmorphing,decorations.text,decorations.markings,fit}
\usepackage[colorinlistoftodos,shadow]{todonotes}

\usepackage{multirow}
\usepackage{mdwlist}

\usepackage{stmaryrd}
\usepackage{mathdots} %for iddots

\usepackage{fancyvrb}

\usepackage[toc,page]{appendix}
\usepackage{float}

\usepackage{extarrows}
\usepackage{pdflscape}
\usepackage{rotating}

\title{Modelling covid 19 - working title}
\author{Leon Wilkosz}
\date{\today{}, Aachen}

\begin{document}
\renewcommand{\figurename}{fig.}
\begin{titlepage}
\maketitle
\tableofcontents
\end{titlepage}


\section{Short term forecasts}
\input{forecast_hub.tex}
In this section we give an overview over a selection of forecast models which where contributed to the european forecast hub \cite{Hub}. 

\subsection{BIOCOMSC-Gompertz}

The group BIOCOMSC-Gompertz from Universitat Politcnica de Catalunya uses the Gompertz function and fits it to current data. It is given by
\[
N(t) = K e^{-ln(\frac{K}{N_0}) \cdot e^{-a(t-t_0)}}
\]
where $N(t)$ is the cumulated number of cases at time $t$ and $N_0$ is the number of cumulated cases at time $t_0$. The two model parameters $a,K$ are fitted with nonlinear least squares method using matlabs curve fitting package. For more information see \cite{catala}

\subsection{DMSPG-Bayes}

The Priesemann group from Max Plank Institute for Dynamics and Self-Organization published their approach in \cite{Priesemann}. For the forecast their model uses an SIR model including a reporting delay. The parameter estimation is done combining Bayesian inference with Monte Carlo sampling. 

\subsubsection{SIR with reporting delay}

With the standard SIR-model given by 
\begin{align*}
\frac{dS}{dt} &= -\lambda \frac{SI}{N} \\
\frac{dI}{dt} &=  \lambda \frac{SI}{N} - \mu I \\
\frac{dR}{dt} &= \mu I
\end{align*}
one calculates the new infections per day via
\[
-I_t^{new} := S_t - S_{t-1}.
\]
With the time constant reporting delay $D$ we obtain the new cases ($C_t$D by
\[
C_t = I_{t-D}^{new}.
\]
However for the forecast $C_t$ is replaced by
\[
C_t = I_{t-D}^{new} (1-f(t))
\]
with
\[
f(t) = (1-f_{\omega})(1 - |\sin(\frac{\pi}{7} - \frac{1}{2} \phi_{\omega}|)
\]
due to modelling the lower case reports on weekends. 

\subsubsection{Parameter estimation}

From the reporting we obtain the real world data $\hat{C_t}$. Given a parameter set
\[
\theta = \{ \lambda, \mu, D, \sigma, I_0, f_{\omega}, \phi_{\omega} \}
\]
we obtain a sequence of modelled cases $\hat{C} = \{C_t(\theta)\}$  where $\sigma$ is the width of the likelihood $p(\hat{C_t}|\theta)$ between the model and the real world data (more on $\sigma$ later). To obtain a suitable set of parameters $\theta$ for the forecast we sample via Markov Chain Monte Carlo method from the posterior distribution
\[
p(\theta|\hat{C})
\]
and use these samples to estimate our $\theta$. Since fore MCMC sampling only a function proportional to $p(\theta|\hat{C})$ is needed we abuse
\[
p(\theta|\hat{C} \; \propto \; p(\hat{C} | \theta) p(\theta)
\]
and compute the latter as follows. The likelihood $p(\hat{C} | \theta)$ is assumed to be the product over the local likelihoods
\[
p(\hat{C_t} | \theta) \sim \text{StudentT}_{\nu = 4}(mean = C_t(\theta), width = \sigma \sqrt{C_t(\theta)})
\]
with
\[
\text{StudentT}_{\nu = 4}(C_t(\theta),\sigma \sqrt{C_t(\theta)})(x) = \frac{\Gamma(\frac{5}{2})}{\Gamma(2)\sqrt{4 \pi \sigma^2 C_t(\theta)}} \left(1 + \frac{1}{4} \left(\frac{x - C_t(\theta)}{\sigma \sqrt{C_t(\theta)}}\right)^2 \right)^{-\frac{5}{2}}.
\]
Moreover the prior $p(\theta)$ is determined by the priors of the single parameters
\begin{align*}
\lambda & \; \sim  \; \text{LogNormal}(\log(0.4),0.5) \\
\mu & \; \sim \; \text{LogNormal}(\log(0.125),0.2) \\
D & \; \sim \; \text{LogNormal}(\log(8),0.5) \\
f_{\omega} & \; \sim \; \text{Beta}(mean = 0.7, std = 0.17) \\
\phi_{\omega} & \; \sim \; \text{vonMises}(mean = 0, \kappa = 0.01) \; \text{(nearly flat)} \\
I_0 & \; \sim \; \text{HalfCauchy}(100) \\
\sigma & \; \sim \; \text{HalfCauchy}(10). \\
\end{align*}
Note that above distributions may have different parameters in the current forecast due to changing political/epidemic situation.

\subsubsection{Longer prediction including policy changes}

Due to policy changes in politics Priesemann group also implemented a version of their model where the infection rate $\lambda$ changes at diskrete time points. The latter are assumed to be normallly distributed. The whole implementation is done in python.

\subsection{Forschungszentrum Jülich and Frankfurt Institute for Advanced Studies}

The model \cite{Jülich} of Forschungszentrum Jülich and Frankfurt Institute for Advanced Studies involved an extended SEIR-model which takes into account that population is structured by age and infection. Therefore three groups are distinguished:
\begin{itemize}
\item children (age 0-14)
\item adults (age 15-59)
\item people 60y and older
\end{itemize}
For an age class A the extended model is given as follows:
\begin{align*}
\dot{S^A}  =&  - \lambda^{A} S^A & \text{susceptibles} \\
\dot{E_1^A}  =& \lambda^A S^A - \gamma_{E_1}^A E_1 &  \text{exposed stage 1}\\
\dot{E_j^A} =& \gamma_{E_{j-1}}^A E_{j-1}^A - \gamma_{E_j}^A E_j^A & \text{exposed stage j = 2,3} \\
\dot{U_1^A} =& (1 - p_0^A) \gamma_{E_3}^A E_3^A - \gamma_{U_1}^A U_1^A & \text{alymptomatic stage 1} \\
\dot{U_j^A} =& (1- \hat{\nu_{j-1}}^A) \gamma_{U_{j-1}}^A U_{j-1}^A - \gamma_{U_j}^A U_j & \text{asymptomatic stage j = 2,3} \\
\dot{I^A_j} = & (1- \nu^A_0) \rho_0^A \gamma_{E_3}^A E_3^A - \gamma_{I_1}^A I_1^A & \text{infected stage 1} \\
\dot{I_j^A} = & (1 - \nu_{j-1}^A) \gamma_{I_{j-1}}^A I_{j-1}^A- \gamma_{I_j}^A I_j^A & \text{infected stage j = 2,3} \\
\dot{H_1^A} =  &\nu_0^A \rho_0^A \gamma_{E_3}^A E_3^A + (1 - v_1^A) \nu_1^A I_1^A - \gamma_{H_1}^A H_1^A & \text{diagnosed stage 1} \\
\dot{H_j^A} = & v_{j-1}^A \nu_{j-1}^A \gamma_{I_{j-1}}^A I_{j-1}^A + \hat{\nu_{j-1}^A} \gamma_{U_{j-1}}^A  U_{j-1}^A & \\
& + (1- v_j^A) \nu_j^A \gamma_{I_j}^A I_j^A + \gamma_{H_{j-1}}^A H_{j-1}^A - \gamma_{H_j}^A H_j^A & \text{diagnosed stage j = 2,3} \\
\dot{R^A} = & (1- \delta_H^A) \gamma_{H_3}^A H_3^A & \text{recovered (detected)} \\
\dot{D^A} = & \delta_H^A \gamma_{H_3}^A H_3^A & \text{deceased} \\
\dot{R_u^A} = & (1 - \nu_3^A) \gamma_{I_3}^A I_3^A + \gamma_U^A U_3^A & \text{recovered (undetected)}
\end{align*}
This model is the fitted to current data.

\subsection{SIkJ$\alpha$-model}
This group from University of Southern California (see \cite{California} uses their onwn developed SIKJ$\alpha$-model for forecasting.

\subsubsection{Baseline assumptions}
The SIKJ$\alpha$ model is non-continuous and divides population into two parts and takes regional differences into account. At region $p$ we have the succeptible group $S_t^{(p)}$ and the infected group $I_t^{(p)}$. Moreover a fraction $\rho^{(p)}$ of the succeptibles is assumed to be immune/isolated. Given the total population $N^{(p)}$ and the infected people $I_t^{(p)}$ at region $p$ this leads to the following equation:
\begin{align}\label{eq:succeptibles}
S_t^{(p)} = (1 - \rho^{(p)}) N^{(p)} - I_t^{(p)}
\end{align}
Moreover assuming an interregional general reporting delay $\lambda$ that local cases are reported with probability $\gamma^{(p)}$ one obtains for the number of reported cases, denoted by $R_t^{(p)}$,
\begin{align}\label{eq:reported}
R_t^{(p)} = \gamma^{(p)} I_{t - \lambda}^{(p)}.
\end{align}
The new infections $\Delta I_t^{(p)}$ determine the dynamics of the epidemic. A covid19 infection is here divided into $k$ time steps which differ in infectiousness. It is assumed that the infectiosness at time t of a person is $\beta_1$ if infected between $t-1$ and $t-J$ and $\beta_2$ if infected between $t-(i-1)J$ and $t-iJ$. Moreover we set $\beta_i = 0$ for $i > k$. Therefore a factor $F(q,p)$ is introduced which represents mobility from region $q$ to region $p$. Moreover the new parameter $\delta$ weights how incoming travel affects the number of infections. Adding the influence from within and outside a region we end up with the following equation for the new infections:
\begin{align}\label{eq:NewInfections}
\Delta I_t^{(p)} = \; & \frac{S_t^{(p)}}{(1 - \rho^{(p)})N^{(p)}} \sum_{i = 1}^{k} \beta_i^{(p)} (I_{t - (i - 1)J}^{(p)} - I_{t - iJ}^{(p)}) \\
& + \delta \sum_{q} F(q,p) \frac{\sum_{i = 1}^{k} \beta_i^{(q)} (I_{t - (i - 1)J}^{(q)} - I_{t - iJ}^{(q)})}{(1 - \rho^{(q)}) N^{(q)}}
\end{align}
Given that a person infected between time $t - (i - 1)J_D$ and $t - iJ_D$ dies with probability $\theta_i^{(p)}$ we derive the equation
\begin{align}\label{deaths}
\Delta_t = \sum_{i = 1}^{k_D} \theta_i^{(p)}(I_{t - (i - 1)J_D} - I_{t - iJ_D}^{(p)}),
\end{align}
where $k_D = k$ and $J_D = J$ are not necessarily required.

\underline{Simplification for travel restrictions}
In case of a rapid rise in the infections travel is mostly restricted and it is reasonable to ignore its influence in \cref{eq:NewInfections}. This gives rise to a simplified correspondence between reported cases and deaths combining \cref{eq:reported} and \cref{eq:NewInfections}:
\begin{align}\label{eq:DeathsSimplified}
\Delta D_t = \sum_{i = 1}^{k_D} \frac{\theta_i^{(p)}}{\gamma_i^{(p)}} ( R_{t + \lambda - (i - 1)J_D} - R_{t + \lambda - iJ_D}).
\end{align}
We write $\overline{\theta}_i^{(p)} = \frac{\theta_i^{(p)}}{\gamma_i^{(p)}}$ and $\overline{\gamma}^{(p)} = \gamma^{(p)}(1 - \rho^{(p)})$.

\subsubsection{Parameter Estimation (simplified model)}
The parameters are clustered in two categories:
\begin{center}
\begin{tabular}{c|c}
learnable parameters & hyperparameters \\
\hline
$\beta_i^{(p)}, \; \overline{\theta}_i^{(p)}$ 	 & $k, \; k_D, \; J, \; J_D, \; \overline{\gamma}^{(p)}$
\end{tabular}
\end{center}
The choice of assuming $\overline{\gamma}^{(p)}$ to be a hyperparameter has a pragmatic reason. Namely it keeps the model linear since in tests a non-linear version performed worse. It is assumed that $\overline{\gamma}^{(p)} = 1/40$. Performing a grid search for $k  \; \in \; \{1, 2\}$ and $J \in \{7,8,...,14\}$, while assuring that $KJ \leq 14$. Moreover a grid search is done for $k_D$, $J_D$ resulting in $k_D = 2$ and $J_D = 7$. $\overline{\beta}_i^{(p)}$ is estimated using weighted least squares on the training data. The death rates are assumed to not evolve very rapidly and can be learned within a window of the last $\omega$ time steps. $\omega$ was found out to perform well at $\omega = 50$. Now $\overline{\theta}_i^{(p)}$ is obtained minimizing the least squared error:
\begin{align*}
LSE_D = \sum_{t = T - \omega + 1}^{T} (\Delta \hat{D}^{(p)}_{t} - \Delta D_t^{(p)})^2.
\end{align*}




\printbibliography


\end{document}