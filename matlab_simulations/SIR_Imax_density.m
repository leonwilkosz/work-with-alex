clear all;
close all;
clf;

y0 = [82999999/83000000 1/83000000 0];
tspan = [0 300];   

beta_mean = 0.185;
gamma_mean = 0.056;

beta_var = 0.03;
gamma_var = 0.01;

mu_beta = log(beta_mean^2/sqrt(beta_mean^2 + beta_var^2));
mu_gamma = log(gamma_mean^2/sqrt(gamma_mean^2 + gamma_var^2));

sigma_beta = log(1 + beta_var^2 / beta_mean^2);
sigma_gamma = log(1 + gamma_var^2 / gamma_mean^2);

samples = 100;

Imax = zeros(1,samples);
indmax = zeros(1,samples);

Rmax = zeros(1,samples);
Rindmax = zeros(1,samples);

beta = lognrnd(mu_beta,sigma_beta,1,samples);
gamma = lognrnd(mu_gamma,sigma_gamma,1,samples);

for i = 1:samples
    DeHandle = @(t,y) SIRmodel(t,y,[beta(i),gamma(i)]);
    [t,y] = ode45(DeHandle, tspan, y0);
    [Imax(i),indmax(i)] = max(y(:,2));
    [Rmax(i),Rindmax(i)] = max(y(:,3));
end

[f,xi] = ksdensity(Imax);
figure(1)
plot(xi,f);
title('SIR Density Imax');

density_fkt = @(t) PolygonalChain(t,xi,f,samples);
mean_fkt = @(t) t*PolygonalChain(t,xi,f,samples);
variance_fkt = @(t) t^2 * PolygonalChain(t,xi,f,samples);


test = integral(density_fkt,0,1,'ArrayValued',true)
mean = integral(mean_fkt,0,1,'ArrayValued',true)
variance = integral(variance_fkt,0,1,'ArrayValued',true) - mean^2
standard_deviation = sqrt(variance)
Belegte_Intensivbetten = mean * 83000000 * 0.026
Intensivbetten = 25037
Abweichung_Belegung = standard_deviation * 83000000 * 0.026


[f_R,xi_R] = ksdensity(Rmax);
figure(2)
plot(xi_R,f_R);
title('SIR Density Rmax');

density_fkt_R = @(t) PolygonalChain(t,xi_R,f_R,samples);
mean_fkt_R = @(t) t*PolygonalChain(t,xi_R,f_R,samples);
variance_fkt_R = @(t) t^2 * PolygonalChain(t,xi_R,f_R,samples);


test_R = integral(density_fkt_R,0,1,'ArrayValued',true)
mean_R = integral(mean_fkt_R,0,1,'ArrayValued',true)
variance_R = integral(variance_fkt_R,0,1,'ArrayValued',true) - mean_R^2
standard_deviation_R = sqrt(variance_R)
Kosten_pro_Intensivbett = 10200
Krankenhauskosten_gesamt = mean_R * 0.026 * 10200 * 83000000
Abweichung_Kosten = standard_deviation_R * 10200 * 0.026 * 83000000

function value = PolygonalChain(t,x,y,steps)
    for i = 1:(steps-1)
        if (x(i) <= t) && (t <= x(i+1))
            value = ((y(i+1)-y(i))/(x(i+1)-x(i)))*(t-x(i)) + y(i);
            return;
        end
    end
    value = 0;
end

function f = SIRmodel(t,y,par) % Funktion zur Erzeugung der Differntialgleichungen des SIR-Modells
    
    % Eingabe: 
    % t,y sind "Platzhalter", damit sp�ter SIRmodel als Funktion in
    % den Variablen t,y geschrieben werden k�nnen
    % par enth�lt die Prameter beta und gamma (in dieser Reihenfolge)
    
    % Wichtig!:
    % Eingegeben wird effektive Kontakrate beta und ausgegeben wird der
    % das SIR-Modell mit dem Transmissionskoeffizienten beta_tilde
    % (Egal, falls N=1 normiert ist)
    
    % Ausgabe:
    % Differentialgleichungs-Vektor mit den 3 Differentialgleichungen
    % entsprechend dem SIR-Modell
    
    beta = par(1);
    gamma = par(2);
    S = y(1);
    I = y(2);
    R = y(3);
    N = S+I+R; % Gr��e der Gesmatbev�lkerung
    
    % Effektive Kontaktrate beta wird zum Transmissionskoeffizienten
    % umgewandelt
    beta_tilde = beta/N; 
    
    % Eingabe der Differentialgleichungen
    Sdot = -beta_tilde *I*S;
    Idot = beta_tilde *I*S - gamma*I;
    Rdot = gamma*I;
    
    % R�ckgabe f
    f = [Sdot Idot Rdot]' ; 
end