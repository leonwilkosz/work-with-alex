
data1 = covid19();
data2 = covid19();
data3 = covid19();

colors = [0, 0.4470, 0.7410; 0.8500, 0.3250, 0.0980; 0.9290, 0.6940, 0.1250].' ; 

[w,l] = size(data1);
N = data1(1,:) + data1(2,:) + data1(3,:) + data1(4,:) + data1(5,:) + data1(6,:);
S = data1(2,:) + data1(3,:);
I = data1(4,:);
R = data1(5,:) + data1(6,:);
x = 1:l;


%using ratio of population as measure
S = S./N;
I = I./N;
R = R./N;
y = [S ; I ; R;]

for e =1:3    
          plot(x,y(e,:),'-',"Color", colors(:,e), "LineWidth", 2);
          hold on;
       end 
legend({'S','I','R'});

[w,l] = size(data2);
N = data2(1,:) + data2(2,:) + data2(3,:) + data2(4,:) + data2(5,:) + data2(6,:);
S = data2(2,:) + data2(3,:);
I = data2(4,:);
R = data2(5,:) + data2(6,:);
x = 1:l;

%using ratio of population as measure
S = S./N;
I = I./N;
R = R./N;
y = [S ; I ; R;]

for e =1:3    
          plot(x,y(e,:),'-',"Color", colors(:,e), "LineWidth", 2);
          hold on;
       end 
legend({'S','I','R'});

[w,l] = size(data3);
N = data3(1,:) + data3(2,:) + data3(3,:) + data3(4,:) + data3(5,:) + data3(6,:);
S = data3(2,:) + data3(3,:);
I = data3(4,:);
R = data3(5,:) + data3(6,:);
x = 1:l;

%using ratio of population as measure
S = S./N;
I = I./N;
R = R./N;
y = [S ; I ; R;]

for e =1:3    
          plot(x,y(e,:),'-',"Color", colors(:,e), "LineWidth", 2);
          hold on;
       end 
legend({'S','I','R'});
title("Simulation with virulence = 0.02 +- 0.002");
