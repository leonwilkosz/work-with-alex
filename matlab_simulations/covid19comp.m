clear all;
clf;
%producing data from simulation and translate into SIR-language
data = covid19(150,10,0,0,0.02,70);
[w,l] = size(data);
N = data(1,:) + data(2,:) + data(3,:) + data(4,:) + data(5,:) + data(6,:);
S = data(2,:) + data(3,:);
I = data(4,:);
R = data(5,:) + data(6,:);
x = 1:l;

%using ratio of population as measure
S = S./N;
I = I./N;
R = R./N;

%estimating parameters for SIR-model via arithmetic mean
gamma = 0;
for i = (31:l-71)
    gamma = gamma + (R(i+1)-R(i))./I(i);
end
gamma = gamma./(l-100);
disp(gamma);

gamma_var = 0;
for i = (31:l-71)
    gamma_var = gamma_var + (gamma - (R(i+1)-R(i))./I(i))^2;
end
gamma_var = sqrt(gamma_var./(l-100));
disp(gamma_var);

beta = 0;
for i = (31:l-71)
    beta = beta + (((I(i+1) - I(i))./I(i))+(R(i+1)-R(i))./I(i))./S(i);
end
beta = beta./(l-100);
disp(beta);

beta_var = 0;
for i = (31:l-71)
    beta_var = beta_var + (beta - (((I(i+1) - I(i))./I(i))+(R(i+1)-R(i))./I(i))./S(i))^2;
end
beta_var = sqrt(beta_var./(l-100));
disp(beta_var);


figure
plot(x,S,x,I,x,R,'linewidth',2);
legend({'S','I','R'});

par = [beta gamma]     % beta (erster Eintrag), gamma (zweiter Eintrag) 
tspan = [0 600];        % Zeitspanne �ber die numerische L�sungen bestimmt werden
y0 = [99/100 1/100 0];  % Anfangsbedingungen

DeHandle = @(t,y) SIRmodel(t,y,par);    % Erzeugen des Funktion-Handels
[t,y] = ode45(DeHandle, tspan, y0);     % Bestimmung numerische N�herungsl�sung

% Erstellung und Anpassung des Plots
figure
plot(t,y,'-', "LineWidth", 2.0);
hold off

figure
hold all;
n = 20;  % Simulationsumfang
tspan = [0,l]; 

% Erzeuge n zuf�llige/unsichere  Parameter 
beta_gitter = beta - (1./4) .*beta_var + (1./2).*beta_var * (1./n).*(1:n);
beta_gitter(beta_gitter <= 0) = 0.001;
gamma_gitter = gamma - (1./2).*gamma_var + gamma_var * rand(1,n);
gamma_gitter(gamma_gitter <= 0)= 0.001;

colors = [0, 0.4470, 0.7410; 0.8500, 0.3250, 0.0980; 0.9290, 0.6940, 0.1250].' ;

% Plotten der L�sungskurven f�r unsichere Daten (gemeinsames Plott-Fenster)
for i = 1:n
       DeHandle = @(t,y) SIRmodel(t,y,[beta_gitter(i) gamma_gitter(i)]);
       [t,y] = ode45(DeHandle, tspan, y0); 
       for e =1:3
          plot(t,y(:,e),'-',"Color", colors(:,e), "LineWidth", 2);
          hold on;
       end 
end

plot(x,S,x,I,x,R,'Color','green','linewidth',2);
legend({'S','I','R'});


function f = SIRmodel(t,y,par) % Funktion zur Erzeugung der Differntialgleichungen des SIR-Modells
    
    % Eingabe: 
    % t,y sind "Platzhalter", damit sp�ter SIRmodel als Funktion in
    % den Variablen t,y geschrieben werden k�nnen
    % par enth�lt die Prameter beta und gamma (in dieser Reihenfolge)
    
    % Wichtig!:
    % Eingegeben wird effektive Kontakrate beta und ausgegeben wird der
    % das SIR-Modell mit dem Transmissionskoeffizienten beta_tilde
    % (Egal, falls N=1 normiert ist)
    
    % Ausgabe:
    % Differentialgleichungs-Vektor mit den 3 Differentialgleichungen
    % entsprechend dem SIR-Modell
    
    beta = par(1);
    gamma = par(2);
    S = y(1);
    I = y(2);
    R = y(3);
    N = S+I+R; % Gr��e der Gesmatbev�lkerung
    
    % Effektive Kontaktrate beta wird zum Transmissionskoeffizienten
    % umgewandelt
    beta_tilde = beta/N; 
    
    % Eingabe der Differentialgleichungen
    Sdot = -beta_tilde *I*S;
    Idot = beta_tilde *I*S - gamma*I;
    Rdot = gamma*I;
    
    % R�ckgabe f
    f = [Sdot Idot Rdot]' ; 
end
