%Here we simulate a situation with a constant percentage of vaccinated
%people

clear all;
close all;
par = [0.3 0.185 0.056 0]     % alpha, beta, gamma, g
tspan = [0 2000];        % time interval for numerical solutions
y0 = [0.3*82999999/83000000 1/83000000 0.7*82999999/83000000 0];  % initial conditons

DeHandle = @(t,y) SIVRmodel(t,y,par); 
[t,y] = ode45(DeHandle, tspan, y0);     % numerical solution

% create and adjust a plot
plot(t,y,'-', "LineWidth", 4.0);
xlabel("Tage")
ylabel("Anteil an der Gesamtbevölkerung")
legend("S","I", "V", "R")
set(gca,'FontSize',15)

hold off

% find the time tmax where I is maximal
[Imax, indmax] = max(y(:,2))
tmax = t(indmax)

rng(2019) % random seed (to make results reproducible

n = 20;  % number of simulations
tspan = [0,2000]; 

% create n random parameters
beta = 0.085 + 0.2 * rand(1,n);
gamma = 0.036 + 0.04 *rand(1,n);
alpha = 0.06 + 0.12 * rand(1,n);
g = zeros(1,n);

% create n random initial conditions
I_0 = (1/83000000)*ones(1,n) + 10^(-4) * rand(1,n);
R_0 = 10^(-4) * rand(1,n);
V_0 = (0.7*82999999/83000000)*ones(1,n) + 10^(-4) * rand(1,n);
S_0 = ones(1,n) - I_0 - R_0 - V_0;
y0 = [S_0; I_0; V_0; R_0];   % Jede Spalte von y_0 entspricht einer zuf. Anfangsbed (S_0, I_0, R_0)

colors = [0, 0.4470, 0.7410; 0.8500, 0.3250, 0.0980; 0.9290, 0.6940, 0.1250; 0.4940, 0.1840, 0.5560].' ; 

figure
% plot curves for random solutions
for i = 1:n
       DeHandle = @(t,y) SIVRmodel(t,y,[alpha(i) beta(i) gamma(i) g(i)]);
       [t,y] = ode45(DeHandle, tspan, y0(:,i)); 
       for e =1:4
          plot(t,y(:,e),'-',"Color", colors(:,e), "LineWidth", 2);
          hold on;
       end 
end

% add curves of mean values
alpha = 0.12;
gamma = 0.056;
beta = 0.185;
g = 0;

I_0 = 1/83000000;
R_0 = 0;
V_0 = 0.7*82999999/83000000;
S_0 = 1 - V_0 - I_0 -R_0;
y0 = [S_0 I_0 V_0 R_0];

tspan = [0 2000];
DeHandle = @(t,y) SIVRmodel(t,y,[alpha beta gamma g]);
[t,y] = ode45(DeHandle, tspan, y0);
plot(t,y,'-',"Color", "green", "LineWidth", 2);

% make plot pretty
L(1) = plot(nan, nan, "Color", colors(:,1), "LineWidth", 2); % Um EW-Kurven in legende wegzulasssen
L(2) = plot(nan, nan, "Color", colors(:,2), "LineWidth", 2);
L(3) = plot(nan, nan, "Color", colors(:,3), "LineWidth", 2);
L(4) = plot(nan, nan, "Color", colors(:,4), "LineWidth", 2);

legend(L, {'S', 'I' 'V', "R"}) % Legende (ohne Eintrag für EW-Kurven)

ylim([0 1])
xlabel("Tage")
ylabel("Anteil an der Gesamtbevölkerung")



set(gca,'FontSize',15)
hold off



%function to create the right side of differential equation
function f = SIVRmodel(t,y,par)
    
    % input: 
    % t,y are the variables
    % par contains beta and gamma
    % important: output is normed to N = 1    
    % output:
    % vector f(y,t) corresponding to equation of SICVR model
    
    alpha = par(1);
    beta = par(2);
    gamma = par(3);
    g = par(4);
    
    S = y(1);
    I = y(2);
    V = y(3);
    R = y(4);
    N = S+I+V+R; % total population
    
    %normalize
    beta_tilde = beta/N; 
    
    % SIVR equation
    Sdot = -beta_tilde *I*S -g*S;
    Idot = beta_tilde *I*S - gamma*I + alpha*beta_tilde*I*V;
    Vdot = g*S - alpha*beta_tilde*I*V;
    Rdot = gamma*I;
    
    % output f
    f = [Sdot Idot Vdot Rdot]' ; 
end