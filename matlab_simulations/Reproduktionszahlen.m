clear all;
close all;
clf;
y0 = [82999999/83000000 1/83000000 0];
tspan = [0 300];   

beta = 0.185;
gamma = 0.056;

DeHandle = @(t,y) SIRmodel(t,y,[beta,gamma]);
[t,y] = ode45(DeHandle, tspan, y0);
R = (beta/gamma).*y(:,1);
figure(1)
plot(t,R);
title("SIR Reproduktionswert")

y0 = [82999999/83000000 0 1/83000000 0];
tspan = [0 400];   

alpha = 0.5;           %Latenzzeit
beta = 0.185;
gamma = 0.056;


function f = SIRmodel(t,y,par) % Funktion zur Erzeugung der Differntialgleichungen des SIR-Modells
    
    % Eingabe: 
    % t,y sind "Platzhalter", damit sp�ter SIRmodel als Funktion in
    % den Variablen t,y geschrieben werden k�nnen
    % par enth�lt die Prameter beta und gamma (in dieser Reihenfolge)
    
    % Wichtig!:
    % Eingegeben wird effektive Kontakrate beta und ausgegeben wird der
    % das SIR-Modell mit dem Transmissionskoeffizienten beta_tilde
    % (Egal, falls N=1 normiert ist)
    
    % Ausgabe:
    % Differentialgleichungs-Vektor mit den 3 Differentialgleichungen
    % entsprechend dem SIR-Modell
    
    beta = par(1);
    gamma = par(2);
    S = y(1);
    I = y(2);
    R = y(3);
    N = S+I+R; % Gr��e der Gesmatbev�lkerung
    
    % Effektive Kontaktrate beta wird zum Transmissionskoeffizienten
    % umgewandelt
    beta_tilde = beta/N; 
    
    % Eingabe der Differentialgleichungen
    Sdot = -beta_tilde *I*S;
    Idot = beta_tilde *I*S - gamma*I;
    Rdot = gamma*I;
    
    % R�ckgabe f
    f = [Sdot Idot Rdot]' ; 
end

function f = SEIRmodel(t,y,par) % Funktion zur Erzeugung der Differntialgleichungen des SIR-Modells
    
    % Eingabe: 
    % t,y sind "Platzhalter", damit sp�ter SIRmodel als Funktion in
    % den Variablen t,y geschrieben werden k�nnen
    % par enth�lt die Prameter beta und gamma (in dieser Reihenfolge)
    
    % Wichtig!:
    % Eingegeben wird effektive Kontakrate beta und ausgegeben wird der
    % das SIR-Modell mit dem Transmissionskoeffizienten beta_tilde
    % (Egal, falls N=1 normiert ist)
    
    % Ausgabe:
    % Differentialgleichungs-Vektor mit den 3 Differentialgleichungen
    % entsprechend dem SIR-Modell
    
    alpha = par(1);
    beta = par(2);
    gamma = par(3);
    S = y(1);
    E = y(2);
    I = y(3);
    R = y(4);
    N = S+E+I+R; % Gr��e der Gesmatbev�lkerung
    
    % Effektive Kontaktrate beta wird zum Transmissionskoeffizienten
    % umgewandelt
    beta_tilde = beta/N; 
    
    % Eingabe der Differentialgleichungen
    Sdot = -beta_tilde *I*S;
    Edot = beta_tilde *I*S - alpha*E;
    Idot = alpha*E - gamma*I;
    Rdot = gamma*I;
    
    % R�ckgabe f
    f = [Sdot Edot Idot Rdot]' ; 
end

function f = SIVRmodel(t,y,par) % Funktion zur Erzeugung der Differntialgleichungen des SIR-Modells
    
    % Eingabe: 
    % t,y sind "Platzhalter", damit sp�ter SIRmodel als Funktion in
    % den Variablen t,y geschrieben werden k�nnen
    % par enth�lt die Prameter beta und gamma (in dieser Reihenfolge)
    
    % Wichtig!:
    % Eingegeben wird effektive Kontakrate beta und ausgegeben wird der
    % das SIR-Modell mit dem Transmissionskoeffizienten beta_tilde
    % (Egal, falls N=1 normiert ist)
    
    % Ausgabe:
    % Differentialgleichungs-Vektor mit den 3 Differentialgleichungen
    % entsprechend dem SIR-Modell

    alpha = par(1);
    beta = par(2);
    gamma = par(3);
    g = par(4);
    
    S = y(1);
    I = y(2);
    V = y(3);
    R = y(4);
    N = S+I+V+R; % Gr��e der Gesmatbev�lkerung
    
    % Effektive Kontaktrate beta wird zum Transmissionskoeffizienten
    % umgewandelt
    beta_tilde = beta/N; 
    
    % Eingabe der Differentialgleichungen
    Sdot = -beta_tilde *I*S -g*S;
    Idot = beta_tilde *I*S - gamma*I + alpha*beta_tilde*I*V;
    Vdot = g*S - alpha*beta_tilde*I*V;
    Rdot = gamma*I;
    
    % R�ckgabe f
    f = [Sdot Idot Vdot Rdot]' ; 
end