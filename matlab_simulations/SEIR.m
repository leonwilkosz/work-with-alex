clear all;
close all force;
clf;
par = [0.5 0.185 0.056]     % beta (erster Eintrag), gamma (zweiter Eintrag) 
tspan = [0 400];        % Zeitspanne �ber die numerische L�sungen bestimmt werden
y0 = [82999999/83000000 0 1/83000000 0];  % Anfangsbedingungen

DeHandle = @(t,y) SEIRmodel(t,y,par);    % Erzeugen des Funktion-Handels
[t,y] = ode45(DeHandle, tspan, y0);     % Bestimmung numerische N�herungsl�sung

% Erstellung und Anpassung des Plots
plot(t,y,'-', "LineWidth", 4.0);
xlabel("Tage (nach 27.01.2020)")
ylabel("Anteil an der Gesamtbev�lkerung")
legend("S","E", "I", "R")
set(gca,'FontSize',15)

hold off

% Bestimmung des Zeitpunkts tmax, zu dem I maximal wird
[Imax,indmax] = max(y(:,3))
tmax = t(indmax)

rng(2019) % Zufalls-Seed (um Ergebnisse reproduzierbar zu machen)

n = 20;  % Simulationsumfang
tspan = [0,400]; 

% Erzeuge n zuf�llige/unsichere  Parameter 
beta = 0.1 + 0.2 * rand(1,n);
gamma = 0.03 + 0.03 *rand(1,n);
alpha = 0.4 + 0.2 * rand(1,n);
% Erzeuge n zuf�llige/unsichere Anfangsbed. Tupel (S_0, I_0, R_0)

I_0 = 0 + 10^(-4) * rand(1,n);
R_0 = 0 + 10^(-4) * rand(1,n);
E_0 = 0 + 10^(-4) * rand(1,n);
S_0 = repmat(1,1,n) - I_0 - R_0 - E_0;
y0 = [S_0; E_0; I_0; R_0];   % Jede Spalte von y_0 entspricht einer zuf. Anfangsbed (S_0, I_0, R_0)

colors = [0, 0.4470, 0.7410; 0.8500, 0.3250, 0.0980; 0.9290, 0.6940, 0.1250; 0.4940, 0.1840, 0.5560].' ; 

figure
% Plotten der L�sungskurven f�r unsichere Daten (gemeinsames Plott-Fenster)
for i = 1:n
       DeHandle = @(t,y) SEIRmodel(t,y,[alpha(i) beta(i) gamma(i)]);
       [t,y] = ode45(DeHandle, tspan, y0(:,i)); 
       for e =1:4
          plot(t,y(:,e),'-',"Color", colors(:,e), "LineWidth", 2);
          hold on;
       end 
end

% Hinzuf�gen der L�sungskurven f�r Erwartungswert Daten
alpha = 0.5;
gamma = (0.03+0.06)/2;
beta = (0.1+0.3)/2;

I_0 = 0.005 * 10^(-2);
R_0 = 0.005 * 10^(-2);
E_0 = 0.005 * 10^(-2);
S_0 = 1 - E_0 - I_0 -R_0;
y0 = [S_0 E_0 I_0 R_0];

tspan = [0 400];
DeHandle = @(t,y) SEIRmodel(t,y,[alpha beta gamma]);
[t,y] = ode45(DeHandle, tspan, y0);
plot(t,y,'-',"Color", "green", "LineWidth", 2);

% Versch�nern des Plot-Fensters
L(1) = plot(nan, nan, "Color", colors(:,1), "LineWidth", 2); % Um EW-Kurven in legende wegzulasssen
L(2) = plot(nan, nan, "Color", colors(:,2), "LineWidth", 2);
L(3) = plot(nan, nan, "Color", colors(:,3), "LineWidth", 2);
L(4) = plot(nan, nan, "Color", colors(:,4), "LineWidth", 2);

legend(L, {'S', 'E' 'I', "R"}) % Legende (ohne Eintrag f�r EW-Kurven)

ylim([0 1])
xlabel("Tage")
ylabel("Anteil an der Gesamtbev�lkerung")



set(gca,'FontSize',15)
hold off


function f = SEIRmodel(t,y,par) % Funktion zur Erzeugung der Differntialgleichungen des SIR-Modells
    
    % Eingabe: 
    % t,y sind "Platzhalter", damit sp�ter SIRmodel als Funktion in
    % den Variablen t,y geschrieben werden k�nnen
    % par enth�lt die Prameter beta und gamma (in dieser Reihenfolge)
    
    % Wichtig!:
    % Eingegeben wird effektive Kontakrate beta und ausgegeben wird der
    % das SIR-Modell mit dem Transmissionskoeffizienten beta_tilde
    % (Egal, falls N=1 normiert ist)
    
    % Ausgabe:
    % Differentialgleichungs-Vektor mit den 3 Differentialgleichungen
    % entsprechend dem SIR-Modell
    
    alpha = par(1);
    beta = par(2);
    gamma = par(3);
    S = y(1);
    E = y(2);
    I = y(3);
    R = y(4);
    N = S+E+I+R; % Gr��e der Gesmatbev�lkerung
    
    % Effektive Kontaktrate beta wird zum Transmissionskoeffizienten
    % umgewandelt
    beta_tilde = beta/N; 
    
    % Eingabe der Differentialgleichungen
    Sdot = -beta_tilde *I*S;
    Edot = beta_tilde *I*S - alpha*E;
    Idot = alpha*E - gamma*I;
    Rdot = gamma*I;
    
    % R�ckgabe f
    f = [Sdot Edot Idot Rdot]' ; 
end